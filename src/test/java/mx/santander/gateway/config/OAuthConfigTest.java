package mx.santander.gateway.config;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {OAuthConfig.class})
@ExtendWith(SpringExtension.class)
class OAuthConfigTest {
    @Autowired
    private OAuthConfig oAuthConfig;

    @Test
    void testOauthServerTemplate() {
        assertTrue(this.oAuthConfig.oauthServerTemplate() instanceof org.springframework.web.client.RestTemplate);
    }
}

