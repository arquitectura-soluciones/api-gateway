package mx.santander.gateway;

import mx.santander.gateway.service.IFirmarJwtService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
public class GatewayApplicationTest {
    @Mock
    private IFirmarJwtService mockFirmarJwtService;

    @InjectMocks
    private GatewayApplication remove1ApplicationUnderTest;

    @Test
    void testAfterPropertiesSet() throws Exception {
        remove1ApplicationUnderTest.afterPropertiesSet();
        verify(mockFirmarJwtService).getRSAJwt();
    }
 
}
