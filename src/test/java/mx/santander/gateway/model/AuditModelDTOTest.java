package mx.santander.gateway.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;

class AuditModelDTOTest {
    @Test
    void testConstructor() {
        AuditModelDTO actualAuditModelDTO = new AuditModelDTO();
        actualAuditModelDTO.setApi("Api");
        actualAuditModelDTO.setClientId("42");
        HashMap<String, List<String>> stringListMap = new HashMap<String, List<String>>(1);
        actualAuditModelDTO.setHeaders(stringListMap);
        actualAuditModelDTO.setMetod("Metod");
        actualAuditModelDTO.setPath("Path");
        HttpHeaders httpHeaders = new HttpHeaders();
        actualAuditModelDTO.setQueryParams(httpHeaders);
        actualAuditModelDTO.setRemoteAddress("42 Main St");
        actualAuditModelDTO.setSubject("Hello from the Dreaming Spires");
        assertEquals("Api", actualAuditModelDTO.getApi());
        assertEquals("42", actualAuditModelDTO.getClientId());
        assertSame(stringListMap, actualAuditModelDTO.getHeaders());
        assertEquals("Metod", actualAuditModelDTO.getMetod());
        assertEquals("Path", actualAuditModelDTO.getPath());
        assertSame(httpHeaders, actualAuditModelDTO.getQueryParams());
        assertEquals("42 Main St", actualAuditModelDTO.getRemoteAddress());
        assertEquals("Hello from the Dreaming Spires", actualAuditModelDTO.getSubject());
    }
}

