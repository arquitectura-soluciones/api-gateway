package mx.santander.gateway.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class ErrorHandlerDTOTest {
    @Test
    void testConstructor() {
        ErrorHandlerDTO actualErrorHandlerDTO = new ErrorHandlerDTO();
        actualErrorHandlerDTO.setCause("Cause");
        actualErrorHandlerDTO.setMessage("Not all who wander are lost");
        assertEquals("Cause", actualErrorHandlerDTO.getCause());
        assertEquals("Not all who wander are lost", actualErrorHandlerDTO.getMessage());
    }
}

