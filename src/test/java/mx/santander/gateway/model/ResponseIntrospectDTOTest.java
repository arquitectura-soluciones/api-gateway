package mx.santander.gateway.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class ResponseIntrospectDTOTest {
    @Test
    void testConstructor() {
        ResponseIntrospectDTO actualResponseIntrospectDTO = new ResponseIntrospectDTO();
        actualResponseIntrospectDTO.setJwt("Jwt");
        assertEquals("Jwt", actualResponseIntrospectDTO.getJwt());
        assertEquals("ResponseIntrospectDTO(jwt=Jwt)", actualResponseIntrospectDTO.toString());
    }
}

