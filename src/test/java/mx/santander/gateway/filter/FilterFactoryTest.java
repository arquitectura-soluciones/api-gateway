package mx.santander.gateway.filter;

import mx.santander.gateway.service.IAuditService;

import static org.hamcrest.CoreMatchers.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.mock.http.server.reactive.MockServerHttpRequest;
import org.springframework.mock.web.server.MockServerWebExchange;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

@ContextConfiguration(classes = {FilterFactory.class})
@ExtendWith(SpringExtension.class)
class FilterFactoryTest {
    @Autowired
    private FilterFactory filterFactory;

    @MockBean
    private IAuditService iAuditService;
    
    @Mock
    GatewayFilterChain filterChain;
    
    @MockBean()
    RestTemplate restTemplate;
    
    @Test
    void testApply() {
        FilterFactory.ArgsFiltersDTO argsFiltersDTO = new FilterFactory.ArgsFiltersDTO();
        argsFiltersDTO.setScope("Scope");
        argsFiltersDTO.getScope();
        
        FilterFactory.ArgsFiltersDTO config = new FilterFactory.ArgsFiltersDTO();
        
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("name", "test1");
        map.put("sex", "male");
        map.put("address", "1324");
        map.put("old", "123");
        
        ResponseEntity<Map> profileMap = ResponseEntity.ok(map);
        Mockito.when(restTemplate.exchange(
        		Mockito.anyString(),
        		Mockito.any(HttpMethod.class),
                Mockito.any(HttpEntity.class),
                Mockito.eq(Map.class)))
        .thenReturn(profileMap);
        
        GatewayFilter filter = this.filterFactory.apply(argsFiltersDTO);
        MockServerHttpRequest expected = MockServerHttpRequest.post("/v1/profile")
                .header("Authorization", "Bearer abd").build();
        MockServerWebExchange exchange = MockServerWebExchange.from(expected);
        filter.filter(exchange, filterChain);
        ServerHttpRequest actual = exchange.getRequest();
    }
    
    @Test
    void testname() {
    	this.filterFactory.name();
    }
}

