package mx.santander.gateway.filter;

import mx.santander.gateway.model.DataJwtDTO;
import mx.santander.gateway.model.ErrorHandlerDTO;
import mx.santander.gateway.model.ResponseIntrospectDTO;
import mx.santander.gateway.service.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.mock.http.server.reactive.MockServerHttpRequest;
import org.springframework.mock.web.server.MockServerWebExchange;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@ExtendWith(MockitoExtension.class)
class PreFilterTest {

	@Mock
	private IRequestIsOkService mockRequestIsOkService;
	@Mock
	private ICustomErrorService mockCustomErrorService;
	@Mock
	private IIntrospectService mockIntrospectServ;
	@Mock
	private IFirmarJwtService mockFirmarJwt;
	@Mock
	private IValidateAccessTokenService mockValidaAccessToken;
	@Mock
	private IAlmacenJwtService mockAlmacenJwt;
	@Mock
	private IAuditService mockAuditService;

	@InjectMocks
	private PreFilter preFilterUnderTest;

	@Test
	void testFilter() {
		try {
			MockServerHttpRequest request = MockServerHttpRequest.get("http://localhost")
					.header(AUTHORIZATION, "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
					.header("Authorization", "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
					.header("X-CF-Forwarded-Url", "https://example.com").build();
			ServerWebExchange exchange = MockServerWebExchange.from(request);
			GatewayFilterChain chain = mock(GatewayFilterChain.class);
			final ErrorHandlerDTO errorHandlerDTO = new ErrorHandlerDTO();
			errorHandlerDTO.setMessage("message");
			errorHandlerDTO.setCause("cause");
			when(mockRequestIsOkService.requestIsOk(any(ServerWebExchange.class))).thenReturn(errorHandlerDTO);
			preFilterUnderTest.filter(exchange, chain);
		} catch (Exception e) {

		}
	}

	@Test
	void testFilter2() {
		try {
			MockServerHttpRequest request = MockServerHttpRequest.get("http://localhost")
					.header(AUTHORIZATION, "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
					.header("Authorization", "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
					.header("X-CF-Forwarded-Url", "https://example.com").build();
			ServerWebExchange exchange = MockServerWebExchange.from(request);
			GatewayFilterChain chain = mock(GatewayFilterChain.class);

			when(mockRequestIsOkService.requestIsOk(any(ServerWebExchange.class))).thenReturn(null);
			when(mockValidaAccessToken.getClientIdFromRequest(any(ServerWebExchange.class))).thenReturn("result");
			when(mockValidaAccessToken.getAccessTokenFromRequest(any(ServerWebExchange.class))).thenReturn("result");
			when(mockValidaAccessToken.tipoAccessToken(any(String.class))).thenReturn("IN");
			when(mockValidaAccessToken.getIssFromRequest(any(ServerWebExchange.class))).thenReturn("result");

			final DataJwtDTO dataJwtDTO = new DataJwtDTO("subject", "tipoPersona", "endPoint");
			dataJwtDTO.setJwt("jwt");
			when(mockAlmacenJwt.estaGuardado(any(DataJwtDTO.class))).thenReturn(dataJwtDTO);
			//when(mockFirmarJwt.sign(any(String.class))).thenReturn("result");

			ResponseIntrospectDTO respIntro = new ResponseIntrospectDTO();
			respIntro.setJwt("jwt");

			/*when(mockIntrospectServ.callInstrospect(any(String.class), any(String.class), any(String.class),
					any(String.class))).thenReturn(respIntro);*/

			preFilterUnderTest.filter(exchange, chain);
		} catch (Exception e) {

		}

	}

	@Test
	void testFilter3() {
		try {
			MockServerHttpRequest request = MockServerHttpRequest.get("http://localhost")
					.header(AUTHORIZATION, "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
					.header("Authorization", "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
					.header("X-CF-Forwarded-Url", "https://example.com").build();
			ServerWebExchange exchange = MockServerWebExchange.from(request);
			GatewayFilterChain chain = mock(GatewayFilterChain.class);

			when(mockRequestIsOkService.requestIsOk(any(ServerWebExchange.class))).thenReturn(null);
			when(mockValidaAccessToken.getClientIdFromRequest(any(ServerWebExchange.class))).thenReturn("result");
			when(mockValidaAccessToken.getAccessTokenFromRequest(any(ServerWebExchange.class))).thenReturn("result");
			when(mockValidaAccessToken.tipoAccessToken(any(String.class))).thenReturn("IN");
			final DataJwtDTO dataJwtDTO = new DataJwtDTO("subject", "tipoPersona", "endPoint");
			dataJwtDTO.setJwt("jwt");
			when(mockAlmacenJwt.estaGuardado(any(DataJwtDTO.class))).thenReturn(dataJwtDTO);
			when(mockValidaAccessToken.getIssFromRequest(any(ServerWebExchange.class))).thenReturn("result");
			//when(mockFirmarJwt.sign(any(String.class))).thenReturn("result");

			ResponseIntrospectDTO respIntro = new ResponseIntrospectDTO();
			respIntro.setJwt("jwt");

			ErrorHandlerDTO error = new ErrorHandlerDTO();
			error.setCause("causa");
			error.setMessage("msj");

			/*when(mockIntrospectServ.callInstrospect(any(String.class), any(String.class), any(String.class),
					any(String.class))).thenReturn(error);*/

			preFilterUnderTest.filter(exchange, chain);
		} catch (Exception e) {

		}
	}

	@Test
	void testFilter4() {
		try {
			MockServerHttpRequest request = MockServerHttpRequest.get("http://localhost")
					.header(AUTHORIZATION, "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
					.header("Authorization", "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
					.header("X-CF-Forwarded-Url", "https://example.com").build();
			ServerWebExchange exchange = MockServerWebExchange.from(request);
			GatewayFilterChain chain = mock(GatewayFilterChain.class);

			when(mockRequestIsOkService.requestIsOk(any(ServerWebExchange.class))).thenReturn(null);
			when(mockValidaAccessToken.getClientIdFromRequest(any(ServerWebExchange.class))).thenReturn("result");
			when(mockValidaAccessToken.getAccessTokenFromRequest(any(ServerWebExchange.class))).thenReturn("result");
			when(mockValidaAccessToken.tipoAccessToken(any(String.class))).thenReturn("IN");
			when(mockValidaAccessToken.getIssFromRequest(any(ServerWebExchange.class))).thenReturn("result");
			when(mockAlmacenJwt.estaGuardado(any(DataJwtDTO.class))).thenReturn(null);
			when(mockFirmarJwt.sign(any(String.class))).thenReturn("result");

			ResponseIntrospectDTO respIntro = new ResponseIntrospectDTO();
			respIntro.setJwt("jwt");

			ErrorHandlerDTO error = new ErrorHandlerDTO();
			error.setCause("causa");
			error.setMessage("msj");

			when(mockIntrospectServ.callInstrospect(any(String.class), any(String.class), any(String.class),
					any(String.class))).thenReturn(error);

			preFilterUnderTest.filter(exchange, chain);
		} catch (Exception e) {

		}

	}
	
	@Test
	void testFilter5() {
		try {
			MockServerHttpRequest request = MockServerHttpRequest.get("http://localhost")
					.header(AUTHORIZATION, "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
					.header("Authorization", "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
					.header("X-CF-Forwarded-Url", "https://example.com").build();
			ServerWebExchange exchange = MockServerWebExchange.from(request);
			GatewayFilterChain chain = mock(GatewayFilterChain.class);

			when(mockRequestIsOkService.requestIsOk(any(ServerWebExchange.class))).thenReturn(null);
			when(mockValidaAccessToken.getClientIdFromRequest(any(ServerWebExchange.class))).thenReturn("result");
			when(mockValidaAccessToken.getAccessTokenFromRequest(any(ServerWebExchange.class))).thenReturn("result");
			when(mockValidaAccessToken.tipoAccessToken(any(String.class))).thenReturn("IN");
			when(mockValidaAccessToken.getIssFromRequest(any(ServerWebExchange.class))).thenReturn("result");
			when(mockAlmacenJwt.estaGuardado(any(DataJwtDTO.class))).thenReturn(null);
			when(mockFirmarJwt.sign(any(String.class))).thenReturn("result");

			ResponseIntrospectDTO respIntro = new ResponseIntrospectDTO();
			respIntro.setJwt("jwt");

			ResponseIntrospectDTO error = new ResponseIntrospectDTO();
			error.setJwt("omar");

			when(mockIntrospectServ.callInstrospect(any(String.class), any(String.class), any(String.class),
					any(String.class))).thenReturn(error);

			preFilterUnderTest.filter(exchange, chain);
		} catch (Exception e) {

		}

	}
}
