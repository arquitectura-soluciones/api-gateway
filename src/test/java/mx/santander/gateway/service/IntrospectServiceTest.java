package mx.santander.gateway.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import mx.santander.gateway.model.ErrorHandlerDTO;
import mx.santander.gateway.model.ResponseIntrospectDTO;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

@ContextConfiguration(classes = { IntrospectService.class, String.class })
@ExtendWith(SpringExtension.class)
class IntrospectServiceTest {
	@Autowired
	private IntrospectService introspectService;

	@MockBean
	private RestOperations restOperations;

	@Test
	void testCallInstrospect() throws RestClientException {
		when(this.restOperations.postForEntity((String) any(), (Object) any(), (Class<Object>) any(), (Object[]) any()))
				.thenThrow(new RestClientException("Msg"));
		assertEquals("instrospect_communication_error",
				((ErrorHandlerDTO) this.introspectService.callInstrospect("Jwt", "ABC123", "Scope", "ABC123"))
						.getCause());
		assertEquals("Msg",
				((ErrorHandlerDTO) this.introspectService.callInstrospect("Jwt", "ABC123", "Scope", "ABC123"))
						.getMessage());
		// verify(this.restOperations).postForEntity((String) any(), (Object) any(),
		// (Class<Object>) any(), (Object[]) any());
	}

	@Test
	void testCallInstrospect2() throws RestClientException {
		ResponseEntity<Object> responseEntity = (ResponseEntity<Object>) mock(ResponseEntity.class);
		when(responseEntity.getBody()).thenThrow(new RestClientException("Msg"));
		when(responseEntity.getStatusCodeValue()).thenReturn(42);
		when(this.restOperations.postForEntity((String) any(), (Object) any(), (Class<Object>) any(), (Object[]) any()))
				.thenReturn(responseEntity);
		assertEquals("instrospect_communication_error",
				((ErrorHandlerDTO) this.introspectService.callInstrospect("Jwt", "ABC123", "Scope", "ABC123"))
						.getCause());
		assertEquals("Msg",
				((ErrorHandlerDTO) this.introspectService.callInstrospect("Jwt", "ABC123", "Scope", "ABC123"))
						.getMessage());
		// verify(this.restOperations).postForEntity((String) any(), (Object) any(),
		// (Class<Object>) any(), (Object[]) any());
		// verify(responseEntity).getBody();
		// verify(responseEntity).getStatusCodeValue();
	}

	@Test
	void testCallInstrospect3() throws RestClientException {
		ResponseEntity<Object> responseEntity = (ResponseEntity<Object>) mock(ResponseEntity.class);
		when(responseEntity.getBody()).thenThrow(new RestClientException("Msg"));
		when(responseEntity.getStatusCodeValue()).thenReturn(42);
		when(this.restOperations.postForEntity((String) any(), (Object) any(), (Class<Object>) any(), (Object[]) any()))
				.thenReturn(responseEntity);
		assertEquals("instrospect_communication_error",
				((ErrorHandlerDTO) this.introspectService.callInstrospect("Jwt", "ABC123", "Scope", "PF")).getCause());
		assertEquals("Msg", ((ErrorHandlerDTO) this.introspectService.callInstrospect("Jwt", "ABC123", "Scope", "PF"))
				.getMessage());
		// verify(this.restOperations).postForEntity((String) any(), (Object) any(),
		// (Class<Object>) any(), (Object[]) any());
		// verify(responseEntity).getBody();
		// verify(responseEntity).getStatusCodeValue();
	}
	
	@Test
	void testCallInstrospect4() throws RestClientException {
		ResponseEntity<Object> responseEntity = (ResponseEntity<Object>) mock(ResponseEntity.class);
		ResponseIntrospectDTO abc=new ResponseIntrospectDTO();
		abc.setJwt("abc");
		when(responseEntity.getBody()).thenReturn(abc);
		when(responseEntity.getStatusCodeValue()).thenReturn(200);
		when(this.restOperations.postForEntity((String) any(), (Object) any(), (Class<Object>) any(), (Object[]) any()))
				.thenReturn(responseEntity);
	}
}
