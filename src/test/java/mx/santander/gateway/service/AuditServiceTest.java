package mx.santander.gateway.service;

import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.server.ServerWebExchange;

import mx.santander.gateway.filter.FilterFactory.ArgsFiltersDTO;

class AuditServiceTest {
    @Test
    void testAudit() {
        AuditService auditService = new AuditService();
        ServerHttpRequest serverHttpRequest = mock(ServerHttpRequest.class);
        when(serverHttpRequest.getQueryParams()).thenReturn(new HttpHeaders());
        when(serverHttpRequest.getHeaders()).thenReturn(new HttpHeaders());
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        when(serverWebExchange.getRequest()).thenReturn(new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(
                new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(serverHttpRequest)))));
        auditService.audit(serverWebExchange, "Hello from the Dreaming Spires");
        verify(serverWebExchange, atLeast(1)).getRequest();
        verify(serverHttpRequest).getHeaders();
        verify(serverHttpRequest).getQueryParams();
    }
    
    @Test
    void testAudit2() {
        AuditService auditService = new AuditService();
        ServerHttpRequest serverHttpRequest = mock(ServerHttpRequest.class);
        when(serverHttpRequest.getQueryParams()).thenReturn(new HttpHeaders());
        when(serverHttpRequest.getHeaders()).thenReturn(new HttpHeaders());
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        when(serverWebExchange.getRequest()).thenReturn(new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(
                new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(serverHttpRequest)))));
        auditService.audit(serverWebExchange);
    }
    
    @Test
    void testAudit3() {
        AuditService auditService = new AuditService();
        ServerHttpRequest serverHttpRequest = mock(ServerHttpRequest.class);
        when(serverHttpRequest.getQueryParams()).thenReturn(new HttpHeaders());
        when(serverHttpRequest.getHeaders()).thenReturn(new HttpHeaders());
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        ArgsFiltersDTO dto=new ArgsFiltersDTO();
        dto.setScope("obv");
        when(serverWebExchange.getRequest()).thenReturn(new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(
                new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(serverHttpRequest)))));
        auditService.audit(serverWebExchange,dto);
    }
    
}

