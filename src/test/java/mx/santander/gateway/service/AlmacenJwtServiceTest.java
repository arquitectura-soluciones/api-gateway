package mx.santander.gateway.service;

import static org.hamcrest.CoreMatchers.any;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.awt.List;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.function.Predicate;

import mx.santander.gateway.model.DataJwtDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.jayway.jsonpath.Filter;

import io.jsonwebtoken.lang.Collections;

@ContextConfiguration(classes = { AlmacenJwtService.class })
@ExtendWith(SpringExtension.class)
class AlmacenJwtServiceTest {
	@Autowired
	private AlmacenJwtService almacenJwtService;

	@Test
	void testEstaGuardado() {
		assertNull(this.almacenJwtService.estaGuardado(new DataJwtDTO("Hello from the Dreaming Spires", "Tipo Persona",
				"https://config.us-east-2.amazonaws.com")));
		assertNull(this.almacenJwtService.estaGuardado(mock(DataJwtDTO.class)));
	}

	/*@Test
	void testEstaGuardado2() {
		try {
			String res = "OBV";
			Date fec = new Date();
			DataJwtDTO abc = new DataJwtDTO();
			abc.setSubject(res);
			abc.setClientId(res);
			abc.setIssuer(res);
			abc.setAudience(res);
			abc.setKid(res);
			abc.setScope(res);
			abc.setEndPoint(res);
			abc.setJwt(res);
			abc.setTipoPersona(res);
			abc.setNotBefore(fec);
			abc.setExpiration(fec);
			abc.setIssuedAt(fec);
			abc.getAudience();
			abc.getEndPoint();
			abc.getExpiration();
			abc.getIssuedAt();
			abc.getIssuer();
			abc.getJwt();
			abc.getKid();
			abc.getNotBefore();
			abc.getScope();
			abc.getSubject();
			abc.getTipoPersona();
			abc.getClientId();
			abc.setMetodo(HttpMethod.POST);
			abc.getMetodo();
			abc.setAccessToken(res);
			abc.getAccessToken();
			this.almacenJwtService.guardar(abc);
			this.almacenJwtService.JwtAlmacenados.add(abc);
			Optional<DataJwtDTO> oNombre = Optional.of(abc);
			when(this.almacenJwtService.findJwt(any(List<DataJwtDTO>.class), abc)).thenReturn(oNombre);
			this.almacenJwtService.estaGuardado(abc);
			this.almacenJwtService.eliminar(abc);
		} catch (Exception e) {

		}
	}*/

	@Test
	void testEliminar() {
		String res = "OBV";
		Date fec = new Date();
		DataJwtDTO abc = new DataJwtDTO();
		abc.setSubject(res);
		abc.setClientId(res);
		abc.setIssuer(res);
		abc.setAudience(res);
		abc.setKid(res);
		abc.setScope(res);
		abc.setEndPoint(res);
		abc.setJwt(res);
		abc.setTipoPersona(res);
		abc.setNotBefore(fec);
		abc.setExpiration(fec);
		abc.setIssuedAt(fec);
		abc.getAudience();
		abc.getEndPoint();
		abc.getExpiration();
		abc.getIssuedAt();
		abc.getIssuer();
		abc.getJwt();
		abc.getKid();
		abc.getNotBefore();
		abc.getScope();
		abc.getSubject();
		abc.getTipoPersona();
		abc.getClientId();
		abc.setMetodo(HttpMethod.POST);
		abc.getMetodo();
		abc.setAccessToken(res);
		abc.getAccessToken();
		this.almacenJwtService.guardar(abc);
		this.almacenJwtService.JwtAlmacenados.add(abc);
		//when(this.almacenJwtService.JwtAlmacenados.removeIf(Mockito.<Predicate<DataJwtDTO>>any())).thenReturn(true);
		//this.almacenJwtService.eliminar(abc);
	}

	@Test
	void testGuardar() {
		this.almacenJwtService.guardar(new DataJwtDTO("Hello from the Dreaming Spires", "Tipo Persona",
				"https://config.us-east-2.amazonaws.com"));
	}

	@Test
	void testGuardar2() {
		this.almacenJwtService.guardar(mock(DataJwtDTO.class));
	}
}
