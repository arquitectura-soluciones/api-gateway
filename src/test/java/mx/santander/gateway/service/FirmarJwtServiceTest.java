package mx.santander.gateway.service;

import static org.hamcrest.CoreMatchers.any;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.PrivateKey;

import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestClientException;

@ContextConfiguration(classes = { FirmarJwtService.class, String.class })
@ExtendWith(SpringExtension.class)
class FirmarJwtServiceTest {

	@TempDir
	Path tempDir;

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@Autowired
	private FirmarJwtService firmarJwtService;

	@Test
	void getRSAJwt() {
		File filex;
		Path pathx;
		PrivateKey publicKey = Mockito.mock(PrivateKey.class);
		try {
			pathx = tempDir.resolve("testfile1.txt");
			filex = pathx.toFile();
			firmarJwtService.getRSAJwt();
		} catch (InvalidPathException ipe) {
			System.err.println("error creating temporary test file in " + this.getClass().getSimpleName());
		}
	}

	@Test
	void testIskeyLoaded() {
		assertFalse(this.firmarJwtService.iskeyLoaded());
	}

	@Test
	void testSign() {
		PrivateKey publicKey = Mockito.mock(PrivateKey.class);
		this.firmarJwtService.llavePrivada=publicKey;
		try {
			String Subject = "Subject";
			String Issuer = "Issuer";
			String Audience = "Audience";
			String Kid = "Audience";
			this.firmarJwtService.sign(Subject, Issuer, Audience, Kid);
		} catch (Exception e) {

		}
	}

	@Test
	void testSign2() {
		PrivateKey publicKey = Mockito.mock(PrivateKey.class);
		try {
			String Subject = "Subject";
			String Issuer = "Issuer";
			String Audience = "Audience";
			String Kid = "Audience";
			this.firmarJwtService.sign(Subject);
		} catch (Exception e) {

		}
	}

	@Test
	void testgetKey() {
		try {
			Path pathx = tempDir.resolve("private-key.pem");
			File filex = pathx.toFile();
			this.firmarJwtService.getKey(filex, "OCHO");
			this.firmarJwtService.getKey(filex, "UNO");
		} catch (Exception e) {

		}
	}

	@Test
	void testGetFormatKey() {
		assertEquals("", this.firmarJwtService
				.getFormatKey(Paths.get(System.getProperty("java.io.tmpdir"), "test.txt").toFile()));
	}

	@Test
	void testGetFormatKey2() {
		Path pathx = tempDir.resolve("private-key.pem");
		this.firmarJwtService.getFormatKey(
				Paths.get(System.getProperty("java.io.tmpdir"), "private-key.pem").toFile());
	}
	
	@Test
	void getFormatKey_unknown() {
		try {
			Path pathx = tempDir.resolve("private-key.pem");
			File filex = pathx.toFile();
			FileOutputStream fos = new FileOutputStream(filex);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			for (int i = 0; i < 10; i++) {
				bw.write("something");
				bw.newLine();
			}
			bw.close();
			this.firmarJwtService.getFormatKey(filex);
		} catch (InvalidPathException ipe) {
			System.err.println("error creating temporary test file in " + this.getClass().getSimpleName());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	void getFormatKey_ocho() {
		try {
			Path pathx = tempDir.resolve("private-key.pem");
			File filex = pathx.toFile();
			FileOutputStream fos = new FileOutputStream(filex);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			for (int i = 0; i < 10; i++) {
				bw.write("BEGIN PRIVATE KEY OBV");
				bw.newLine();
			}
			bw.close();
			this.firmarJwtService.getFormatKey(filex);
		} catch (InvalidPathException ipe) {
			System.err.println("error creating temporary test file in " + this.getClass().getSimpleName());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	void formatPkcsOcho() {
		try {
			Path pathx = tempDir.resolve("private-key.pem");
			File filex = pathx.toFile();
			FileOutputStream fos = new FileOutputStream(filex);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			String conte="-----BEGIN PRIVATE KEY-----\r\n"
					+ "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCx4FDiEqaDrqV4\r\n"
					+ "m2vk5IXbt1Ocr72hpfQ3SUl5eihNnt8IsbiEycX14qlqdjfz3J2hNWgjvSwJj4tc\r\n"
					+ "AZJqqNAxj9Csm/RIM+7ngmLQdx3Ix4+TIj2XmsjMYXDWfPenuAbo+UOQJAOq3Fwa\r\n"
					+ "LBXYHG1qrGMo0yP29VZU7XEEwHzllQwWy+TKSA2ePxHxaYT6TzsC5hyuOPh4ntxk\r\n"
					+ "8RF2t2sV7hg/3BA/eu1F7D0TdW++JDcrZHIY1a9Oplm1nQA9+oNFH5lgcZ++JV9K\r\n"
					+ "haAg0UWFGOeB3bk88zqMu68t2WlUy9n6K/a52JteuQKasPj8tfq+0WKrnWfoYT3G\r\n"
					+ "mmo7eI/JAgMBAAECggEBAIn4ZoSU1tf5VZxRYcUvhKzCbHZy0nJaGS3xCK7TFAPx\r\n"
					+ "ISrLP0awg36QC7zqacgb7aCnnllqWIgp8XyyM3HL84Ed/kyEw1xl+/9S4Xa1jkK4\r\n"
					+ "mCMWQhQgGXR+eFnGoQpU8fcvsTpS1Q1o03/xhhgVJMHDaT63V4QtFIPXkPyQmdAx\r\n"
					+ "Gs3wl7txl09Fbq+UKmUQHu6UrdhyXGvWpudNlAIrCVgA9/gZsQuA0ZrK1FJ6gPyF\r\n"
					+ "EJQaFtO6OVjekeUlQ6196MLqb1Os1tqlPYmu0At13rxY3BtPH1TnXocNNYnSajFt\r\n"
					+ "W5pr7hxrUHvyrIQDAlDkT9v9OJSzz/E/TFj8BfNIawECgYEA3ujqyOlhbEzohRet\r\n"
					+ "FWrLigUP7mC86PvMxV4vqmsHpMRJPOlsqX3KaFa2kxWTFML+tVRd6dQc5uvI/JSB\r\n"
					+ "+RdUlZHaG2oHNUfTV3NtjI/elO5xA8XL9w3MlmYkjMMufF+yU8E7hZNIzx0ByDE8\r\n"
					+ "ADZYIn7Fiso04EpHxhvsgMCp99ECgYEAzEgE6qBSmSKKMxqbBFuuarSio+r1kyXS\r\n"
					+ "yo9cDSyqyp/fIi2++XgqJKXnRzLweA3CYhO54TSCuyLROSIoRLo0fH82nU5afUd+\r\n"
					+ "x5OCWPNpjGimIoCjbTd3hbDew3Z2VF4P8M4wPtRELDn269ER6XnbRH4QbukXXciB\r\n"
					+ "pOihXiCFDnkCgYA3EfYHQtklx25eizXgp6y3YX7JFq81JBg33ogKIz0VMMUjPkvx\r\n"
					+ "J7mcw2eBuP/ITlBc/gXhodyuFCxockwV/PSRqyHmSfCT8jW9UjALXPPReYMEOpak\r\n"
					+ "Z41Wzs9S01mlsoK8/G03F+cR/AmXqso3BzYI96bYHhKoItHOmNF6EZ8sMQKBgQC4\r\n"
					+ "fPZLuxy9he8PtiB3sCD150rXBNRh3w/etdfpYgSg1/9ZS70kafyNenND3uJDWhHK\r\n"
					+ "J4yAsb67KPeRRKLfZtcN9ImrSV75s37w2OwoTzRshPo9QLXXZfeAerrEBhFZolV9\r\n"
					+ "IYl6xJMI6hRw5jDqDY4T7lIDCGl83h8PgrRxYKcp6QKBgQCir7lSvGODODCb0EL2\r\n"
					+ "t8TZnxrkVqN1d7pDrmncmxoJ4Ryl/HDg2a8FhO5lWQJCKgdtzHlQwfEFFxg0eHiL\r\n"
					+ "Jky0T53zawqaG3hMKPHzKLMsK4fJ4qLw+ueBQA4BM7TeCZPHdeEhK43Bj8xKZE4+\r\n"
					+ "pt222oyVSHPzyQ/ArGZr1jHBZA==\r\n"
					+ "-----END PRIVATE KEY-----\r\n"
					+ "";
				bw.write(conte);
				bw.newLine();
			bw.close();
			//this.firmarJwtService.formatPkcsOcho(filex);
		} catch (InvalidPathException ipe) {
			System.err.println("error creating temporary test file in " + this.getClass().getSimpleName());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	void formatPkcsUnoTest() {
		try {
			Path pathx = tempDir.resolve("private-key.pem");
			File filex = pathx.toFile();
			FileOutputStream fos = new FileOutputStream(filex);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			String conte="-----BEGIN RSA PRIVATE KEY-----\r\n"
					+ "MIIEpAIBAAKCAQEA4fB2tgelVncTBZkaaXZ8LHxqxxBTf5QH1P+0kV6BfO02Y8Wn\r\n"
					+ "oKspw0BNK7no/4V3A70IvYBOamNECtK0f9h+wAT5JTIAx2oErtnFz/gXffuFvrKT\r\n"
					+ "hKd/m/3b9LC/kHpyq7+gpfD0vW4Je61oBznKzR09giM0MTfxo+F9eMnggIwv4kh8\r\n"
					+ "UKUoG4MFmGmEnQD8yM0m59QllVIg/qPMntUpkjwg5cdw/vRoLGIQudwa6CPH/6Q7\r\n"
					+ "yjcZO9kWMwns1tqLUTQKll5jaSlNUmcAppcgGOPOwksyy5cabqb9/qKaBQWA+Cos\r\n"
					+ "kyBcWM6XTacpQXTQiLZiMVWqrfxxt22tu8PTJQIDAQABAoIBAFAmruY6ZiP7T+sv\r\n"
					+ "ehgpqTla1cJSf1yF1hVwtMzRGtyHhkD0sFemAMW8POQjKo3i+eqAl+Yp6yeAmXXB\r\n"
					+ "c7jaw3Q5D21rhK05ZfnlVXBGkYbmwHZ7ASwRA4sCDpKKVRra5W0aKNJVXW0J6SFP\r\n"
					+ "zdSqYtBK7DQLawt+ziEV8B32d8cByYqb4q0wLq0V1qUEBq3fCbzixCU9OWoZJsBs\r\n"
					+ "1W73OoOLyv+Y3vdfVShYKpLHnqBAs3Wjx0WD6sM5wv90jAw2NyhrDq9p5YDYNLy8\r\n"
					+ "jR5GP1nBR+VfOZy55UqYwoYvIUq77ug+ad8cpllGAGdqh6hBfvhaQjlm470lOujp\r\n"
					+ "QwMQ0U0CgYEA+NsX6EeR3UkGrZ0+rJgkIbZejvhfmYUbBQ6yq1VgW8b/H2Fmxw7g\r\n"
					+ "pJ6YfT/O8hWvfT7X3xlzQpQHNtV/GTpHTEn8vIS2vRx53hFBpSr3Fs7oAsHF0QBV\r\n"
					+ "lj4it86pMXlt9P+PJ7AAFjNGqOtQ43/JulXPhlcO9pUL5Lk3cEgF2QsCgYEA6Gzz\r\n"
					+ "aGLZ+CWgNdCc0wsFZ0SMr9RliBiC8fHzVXXYoywDOVnJoKy33o6TnznxIaTogfWr\r\n"
					+ "7Im0wtVxzuETjvhlGBXkQdSO5bR9x0/XvaZgsV1ErxzGRXZuTIAVUA3X37t3YDuK\r\n"
					+ "DK1oVmDEfEf85L/+L5p4ir9oVIqnJjlDYJXYgo8CgYEAwdO8MkIUXXu0hOM13q59\r\n"
					+ "bfiie87+fw+KdQz2orZT4AgFzAZMS/zNgcq5UMyzPiEf13hsISusd43edJojbZ3T\r\n"
					+ "zJrQDHF995vHW+yq6Z+mZpBvaz5L+ja9ctEgr3N6mrtOpoWqlani12fGwGjQTvHI\r\n"
					+ "SKbvhV4nnG2InvSYNo8SZvMCgYAngf2ODN+IMr0c6Kz/oqv0dc6ERNJNY/0+KyZ1\r\n"
					+ "rGZ6tgyGPyDq0vEeP5YQRsUaNvHexWfH+VBkneuOguvULZzhjzcRiZQQr7GBo6zE\r\n"
					+ "a/ebroLyUS3SuOLe1hbJWNENEsyj6vr4VLGbnS9JAOOX7prCdLtwukB9vmPTN4Cg\r\n"
					+ "dlgrOQKBgQCUQKHzWcfaF8Mm5VI3eHPY5Qi0U8NQHKDMPt+VWOY0qtmxP/ugomRL\r\n"
					+ "qpUG6J/F5DZ9Wgqvm3xxA8injdJ/OiXCI54W0t/GtSzzSIw0/EcVRRl+Ycs2TT3x\r\n"
					+ "34r/H7dV1KFICOa/9Ro0e9zuY8PHzX6GA/Hs4YnJcEGZCfU1B9vE/A==\r\n"
					+ "-----END RSA PRIVATE KEY-----\r\n"
					+ "";
				bw.write(conte);
				bw.newLine();
			bw.close();
			this.firmarJwtService.formatPkcsUno(filex);
		} catch (InvalidPathException ipe) {
			System.err.println("error creating temporary test file in " + this.getClass().getSimpleName());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	void formatPkcsUnoTest2() {
		try {
			Path pathx = tempDir.resolve("private-key.pem");
			File filex = pathx.toFile();
			FileOutputStream fos = new FileOutputStream(filex);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			String conte=null;
//				bw.write(conte);
//				bw.newLine();
			bw.close();
			this.firmarJwtService.formatPkcsUno(filex);
		} catch (InvalidPathException ipe) {
			ipe.printStackTrace();
			System.err.println("error creating temporary test file in " + this.getClass().getSimpleName());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	void getFormatKey_UNO() {
		try {
			Path pathx = tempDir.resolve("private-key.pem");
			File filex = pathx.toFile();
			FileOutputStream fos = new FileOutputStream(filex);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			for (int i = 0; i < 10; i++) {
				bw.write("BEGIN RSA PRIVATE KEY OBV");
				bw.newLine();
			}
			bw.close();
			this.firmarJwtService.getFormatKey(filex);
		} catch (InvalidPathException ipe) {
			System.err.println("error creating temporary test file in " + this.getClass().getSimpleName());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	void testGetKey() {
		assertNull(this.firmarJwtService.getKey(Paths.get(System.getProperty("java.io.tmpdir"), "test.txt").toFile(),
				"OCHO"));
	}

	@Test
	void testFormatPkcsOcho() {
		assertNull(this.firmarJwtService
				.formatPkcsOcho(Paths.get(System.getProperty("java.io.tmpdir"), "test.txt").toFile()));
	}
	
	@Test
	void testFormatPkcsOchox() {
		Path pathx = tempDir.resolve("testfile1.txt");
		File filex = pathx.toFile();
		this.firmarJwtService.formatPkcsOcho(filex);
	}

}
