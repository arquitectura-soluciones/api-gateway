package mx.santander.gateway.service;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.function.Function;

import mx.santander.gateway.model.ErrorHandlerDTO;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ChannelSendOperator;
import org.springframework.http.server.reactive.HttpHeadResponseDecorator;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.mock.http.server.reactive.MockServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;

class CustomErrorServiceTest {
    @Test
    void testErrorResponse() {
        CustomErrorService customErrorService = new CustomErrorService();
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        when(serverWebExchange.getResponse()).thenReturn(new MockServerHttpResponse());

        ErrorHandlerDTO errorHandlerDTO = new ErrorHandlerDTO();
        errorHandlerDTO.setMessage("Not all who wander are lost");
        errorHandlerDTO.setCause("Cause");
        customErrorService.errorResponse(serverWebExchange, errorHandlerDTO);
        verify(serverWebExchange).getResponse();
    }

    @Test
    void testErrorResponse2() {
        CustomErrorService customErrorService = new CustomErrorService();
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        when(serverWebExchange.getResponse()).thenReturn(new MockServerHttpResponse(new DefaultDataBufferFactory()));

        ErrorHandlerDTO errorHandlerDTO = new ErrorHandlerDTO();
        errorHandlerDTO.setMessage("Not all who wander are lost");
        errorHandlerDTO.setCause("Cause");
        customErrorService.errorResponse(serverWebExchange, errorHandlerDTO);
        verify(serverWebExchange).getResponse();
    }

    @Test
    void testErrorResponse3() {
        CustomErrorService customErrorService = new CustomErrorService();
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        when(serverWebExchange.getResponse()).thenReturn(new HttpHeadResponseDecorator(new MockServerHttpResponse()));

        ErrorHandlerDTO errorHandlerDTO = new ErrorHandlerDTO();
        errorHandlerDTO.setMessage("Not all who wander are lost");
        errorHandlerDTO.setCause("Cause");
        customErrorService.errorResponse(serverWebExchange, errorHandlerDTO);
        verify(serverWebExchange).getResponse();
    }

    @Test
    void testErrorResponse4() {
        CustomErrorService customErrorService = new CustomErrorService();
        ServerHttpResponse serverHttpResponse = mock(ServerHttpResponse.class);
        when(serverHttpResponse.writeWith((Publisher<? extends org.springframework.core.io.buffer.DataBuffer>) any()))
                .thenReturn(new ChannelSendOperator((Publisher<Object>) mock(Publisher.class),
                        (Function<Publisher<Object>, Publisher<Void>>) mock(Function.class)));
        when(serverHttpResponse.bufferFactory()).thenReturn(new DefaultDataBufferFactory());
        when(serverHttpResponse.getHeaders()).thenReturn(new HttpHeaders());
        when(serverHttpResponse.setStatusCode((org.springframework.http.HttpStatus) any())).thenReturn(true);
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        when(serverWebExchange.getResponse()).thenReturn(serverHttpResponse);

        ErrorHandlerDTO errorHandlerDTO = new ErrorHandlerDTO();
        errorHandlerDTO.setMessage("Not all who wander are lost");
        errorHandlerDTO.setCause("Cause");
        customErrorService.errorResponse(serverWebExchange, errorHandlerDTO);
        verify(serverWebExchange).getResponse();
        verify(serverHttpResponse).bufferFactory();
        verify(serverHttpResponse).getHeaders();
        verify(serverHttpResponse).setStatusCode((org.springframework.http.HttpStatus) any());
        verify(serverHttpResponse).writeWith((Publisher<? extends org.springframework.core.io.buffer.DataBuffer>) any());
    }
}

