package mx.santander.gateway.service;

import mx.santander.gateway.model.ErrorHandlerDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.http.server.reactive.MockServerHttpRequest;
import org.springframework.mock.web.server.MockServerWebExchange;
import org.springframework.web.server.ServerWebExchange;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@ExtendWith(MockitoExtension.class)
class RequestIsOkServiceTest {

    @Mock
    private IValidateAccessTokenService mockValidaAccessToken;
    @Mock
    private IFirmarJwtService mockFirmarJwt;

    @InjectMocks
    private RequestIsOkService requestIsOkServiceUnderTest;

    @Test
    void testRequestIsOk() {
    	MockServerHttpRequest request = MockServerHttpRequest.get("http://localhost")
				.header(AUTHORIZATION, "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
				.header("Authorization", "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
				.header("X-CF-Forwarded-Url", "https://example.com").build();
		ServerWebExchange exchange = MockServerWebExchange.from(request);
        when(mockValidaAccessToken.getAccessTokenFromRequest(any(ServerWebExchange.class))).thenReturn("result");
        when(mockValidaAccessToken.tipoAccessToken(any(String.class))).thenReturn("IN");
        when(mockValidaAccessToken.getIssFromRequest(any(ServerWebExchange.class))).thenReturn("result");
        when(mockValidaAccessToken.getClientIdFromRequest(any(ServerWebExchange.class))).thenReturn("result");
        when(mockFirmarJwt.iskeyLoaded()).thenReturn(false);
        final ErrorHandlerDTO result = requestIsOkServiceUnderTest.requestIsOk(exchange);
    }
    
    @Test
    void testRequestIsOk2() {
    	MockServerHttpRequest request = MockServerHttpRequest.get("http://localhost")
				.header(AUTHORIZATION, "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
				.header("Authorization", "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
				.header("X-CF-Forwarded-Url", "https://example.com").build();
		ServerWebExchange exchange = MockServerWebExchange.from(request);
        when(mockValidaAccessToken.getAccessTokenFromRequest(any(ServerWebExchange.class))).thenReturn("result");
        when(mockValidaAccessToken.tipoAccessToken(any(String.class))).thenReturn(null);
        when(mockValidaAccessToken.getIssFromRequest(any(ServerWebExchange.class))).thenReturn("result");
        when(mockValidaAccessToken.getClientIdFromRequest(any(ServerWebExchange.class))).thenReturn("result");
        when(mockFirmarJwt.iskeyLoaded()).thenReturn(false);
        final ErrorHandlerDTO result = requestIsOkServiceUnderTest.requestIsOk(exchange);
    }
    
    @Test
    void testRequestIsOk3() {
    	MockServerHttpRequest request = MockServerHttpRequest.get("http://localhost")
				.header(AUTHORIZATION, "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
				.header("Authorization", "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
				.header("X-CF-Forwarded-Url", "https://example.com").build();
		ServerWebExchange exchange = MockServerWebExchange.from(request);
        when(mockValidaAccessToken.getAccessTokenFromRequest(any(ServerWebExchange.class))).thenReturn("result");
        when(mockValidaAccessToken.tipoAccessToken(any(String.class))).thenReturn(null);
        when(mockValidaAccessToken.getIssFromRequest(any(ServerWebExchange.class))).thenReturn(null);
        when(mockValidaAccessToken.getClientIdFromRequest(any(ServerWebExchange.class))).thenReturn("result");
        when(mockFirmarJwt.iskeyLoaded()).thenReturn(false);
        final ErrorHandlerDTO result = requestIsOkServiceUnderTest.requestIsOk(exchange);
    }

    @Test
    void testRequestIsOk5() {
    	MockServerHttpRequest request = MockServerHttpRequest.get("http://localhost")
				.header(AUTHORIZATION, "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
				.header("Authorization", "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
				.header("X-CF-Forwarded-Url", "https://example.com").build();
		ServerWebExchange exchange = MockServerWebExchange.from(request);
        when(mockValidaAccessToken.getAccessTokenFromRequest(any(ServerWebExchange.class))).thenReturn("result");
        when(mockValidaAccessToken.tipoAccessToken(any(String.class))).thenReturn(null);
        when(mockValidaAccessToken.getIssFromRequest(any(ServerWebExchange.class))).thenReturn(null);
        when(mockValidaAccessToken.getClientIdFromRequest(any(ServerWebExchange.class))).thenReturn(null);
        when(mockFirmarJwt.iskeyLoaded()).thenReturn(true);
        final ErrorHandlerDTO result = requestIsOkServiceUnderTest.requestIsOk(exchange);
    }
    
    @Test
    void testRequestIsOk6() {
    	MockServerHttpRequest request = MockServerHttpRequest.get("http://localhost")
				.header(AUTHORIZATION, "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
				.header("Authorization", "Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=")
				.header("X-CF-Forwarded-Url", "https://example.com").build();
		ServerWebExchange exchange = MockServerWebExchange.from(request);
        when(mockValidaAccessToken.getAccessTokenFromRequest(any(ServerWebExchange.class))).thenReturn(null);
   
        final ErrorHandlerDTO result = requestIsOkServiceUnderTest.requestIsOk(exchange);
    }
}
