package mx.santander.gateway.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ServerWebExchange;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@ContextConfiguration(classes = {ValidateAccessTokenService.class, String.class})
@ExtendWith(SpringExtension.class)
class ValidateAccessTokenServiceTest {
    @Autowired
    private ValidateAccessTokenService validateAccessTokenService;

    @Test
    void testGetAccessTokenFromRequest() {
        ValidateAccessTokenService validateAccessTokenService = new ValidateAccessTokenService();
        ServerHttpRequest serverHttpRequest = mock(ServerHttpRequest.class);
        when(serverHttpRequest.getHeaders()).thenReturn(new HttpHeaders());
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        when(serverWebExchange.getRequest()).thenReturn(new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(
                new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(serverHttpRequest)))));
        assertNull(validateAccessTokenService.getAccessTokenFromRequest(serverWebExchange));
        verify(serverWebExchange).getRequest();
        verify(serverHttpRequest).getHeaders();
    }
    
    @Test
    void testGetAccessTokenFromRequest2() {
        ValidateAccessTokenService validateAccessTokenService = new ValidateAccessTokenService();
        ServerHttpRequest serverHttpRequest = mock(ServerHttpRequest.class);
        HttpHeaders head=new HttpHeaders();
        head.add(AUTHORIZATION, "Bearer obv");
        when(serverHttpRequest.getHeaders()).thenReturn(head);
        
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        when(serverWebExchange.getRequest()).thenReturn(new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(
                new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(serverHttpRequest)))));
    }
    
    @Test
    void testGetAccessTokenFromRequest3() {
        ValidateAccessTokenService validateAccessTokenService = new ValidateAccessTokenService();
        ServerHttpRequest serverHttpRequest = mock(ServerHttpRequest.class);
        HttpHeaders head=new HttpHeaders();
        head.add(AUTHORIZATION, "Bearer obv");
        when(serverHttpRequest.getHeaders()).thenReturn(null);
        
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        when(serverWebExchange.getRequest()).thenReturn(new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(
                new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(serverHttpRequest)))));
    }

    @Test
    void testGetIssFromRequest() {
        ServerHttpRequest serverHttpRequest = mock(ServerHttpRequest.class);
        when(serverHttpRequest.getHeaders()).thenReturn(new HttpHeaders());
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        when(serverWebExchange.getRequest()).thenReturn(new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(
                new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(serverHttpRequest)))));
        assertNull(this.validateAccessTokenService.getIssFromRequest(serverWebExchange));
        verify(serverWebExchange).getRequest();
        verify(serverHttpRequest).getHeaders();
    }

    @Test
    void testTipoAccessToken() {
        assertNull(this.validateAccessTokenService.tipoAccessToken("ABC123"));
        assertEquals("IN", this.validateAccessTokenService.tipoAccessToken("IN"));
        assertEquals("PF", this.validateAccessTokenService.tipoAccessToken("PF"));
        assertNull(this.validateAccessTokenService.tipoAccessToken(""));
    }

    @Test
    void testGetClientIdFromRequest() {
        ServerHttpRequest serverHttpRequest = mock(ServerHttpRequest.class);
        when(serverHttpRequest.getHeaders()).thenReturn(new HttpHeaders());
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        when(serverWebExchange.getRequest()).thenReturn(new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(
                new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(serverHttpRequest)))));
        assertNull(this.validateAccessTokenService.getClientIdFromRequest(serverWebExchange));
        verify(serverWebExchange).getRequest();
        verify(serverHttpRequest).getHeaders();
    }
    
    @Test
    void testGetClientIdFromRequest2() {
        ServerHttpRequest serverHttpRequest = mock(ServerHttpRequest.class);
        HttpHeaders head=new HttpHeaders();
        head.add("x-ibm-client-id", "");
        
        when(serverHttpRequest.getHeaders()).thenReturn(head);
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        when(serverWebExchange.getRequest()).thenReturn(new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(
                new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(serverHttpRequest)))));
    }
    
    @Test
    void testGetClientIdFromRequest3() {
        ServerHttpRequest serverHttpRequest = mock(ServerHttpRequest.class);
        HttpHeaders head=new HttpHeaders();
        head.add("x-ibm-client-id", null);
        
        when(serverHttpRequest.getHeaders()).thenReturn(head);
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        when(serverWebExchange.getRequest()).thenReturn(new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(
                new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(serverHttpRequest)))));
    }
    
    @Test
    void testGetClientIdFromRequest4() {
        ServerHttpRequest serverHttpRequest = mock(ServerHttpRequest.class);
        when(serverHttpRequest.getHeaders()).thenReturn(null);
        ServerWebExchange serverWebExchange = mock(ServerWebExchange.class);
        when(serverWebExchange.getRequest()).thenReturn(new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(
                new ServerHttpRequestDecorator(new ServerHttpRequestDecorator(serverHttpRequest)))));
    }
    
    @Test
    void testCreateBasicAuthHeader() {
        assertEquals("Basic amFuZWRvZTppbG92ZXlvdQ==",
                this.validateAccessTokenService.createBasicAuthHeader("janedoe", "iloveyou"));
    }
}

