package mx.santander.gateway.exception;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import org.junit.jupiter.api.Test;
import org.springframework.boot.web.reactive.context.AnnotationConfigReactiveWebApplicationContext;
import org.springframework.http.codec.support.DefaultServerCodecConfigurer;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.support.ServerRequestWrapper;

class GlobalErrorWebExceptionHandlerTest {
    @Test
    void testRenderErrorResponse() {
        GlobalErrorAttributes globalErrorAttributes = mock(GlobalErrorAttributes.class);
        when(globalErrorAttributes.getErrorAttributes((ServerRequest) any(),
                (org.springframework.boot.web.error.ErrorAttributeOptions) any())).thenReturn(new HashMap<String, Object>(1));
        AnnotationConfigReactiveWebApplicationContext applicationContext = new AnnotationConfigReactiveWebApplicationContext();
        GlobalErrorWebExceptionHandler globalErrorWebExceptionHandler = new GlobalErrorWebExceptionHandler(
                globalErrorAttributes, applicationContext, new DefaultServerCodecConfigurer());
        globalErrorWebExceptionHandler.renderErrorResponse(new ServerRequestWrapper(
                new ServerRequestWrapper(new ServerRequestWrapper(new ServerRequestWrapper(mock(ServerRequest.class))))));
        verify(globalErrorAttributes).getErrorAttributes((ServerRequest) any(),
                (org.springframework.boot.web.error.ErrorAttributeOptions) any());
    }

    @Test
    void testRenderErrorResponse2() {
        GlobalErrorAttributes globalErrorAttributes = mock(GlobalErrorAttributes.class);
        when(globalErrorAttributes.getErrorAttributes((ServerRequest) any(),
                (org.springframework.boot.web.error.ErrorAttributeOptions) any())).thenReturn(new HashMap<String, Object>(0));
        AnnotationConfigReactiveWebApplicationContext applicationContext = new AnnotationConfigReactiveWebApplicationContext();
        GlobalErrorWebExceptionHandler globalErrorWebExceptionHandler = new GlobalErrorWebExceptionHandler(
                globalErrorAttributes, applicationContext, new DefaultServerCodecConfigurer());
        globalErrorWebExceptionHandler.renderErrorResponse(new ServerRequestWrapper(
                new ServerRequestWrapper(new ServerRequestWrapper(new ServerRequestWrapper(mock(ServerRequest.class))))));
        verify(globalErrorAttributes).getErrorAttributes((ServerRequest) any(),
                (org.springframework.boot.web.error.ErrorAttributeOptions) any());
    }

    @Test
    void testRenderErrorResponse3() {
        HashMap<String, Object> stringObjectMap = new HashMap<String, Object>(1);
        stringObjectMap.put("foo", "42");
        GlobalErrorAttributes globalErrorAttributes = mock(GlobalErrorAttributes.class);
        when(globalErrorAttributes.getErrorAttributes((ServerRequest) any(),
                (org.springframework.boot.web.error.ErrorAttributeOptions) any())).thenReturn(stringObjectMap);
        AnnotationConfigReactiveWebApplicationContext applicationContext = new AnnotationConfigReactiveWebApplicationContext();
        GlobalErrorWebExceptionHandler globalErrorWebExceptionHandler = new GlobalErrorWebExceptionHandler(
                globalErrorAttributes, applicationContext, new DefaultServerCodecConfigurer());
        globalErrorWebExceptionHandler.renderErrorResponse(new ServerRequestWrapper(
                new ServerRequestWrapper(new ServerRequestWrapper(new ServerRequestWrapper(mock(ServerRequest.class))))));
        verify(globalErrorAttributes).getErrorAttributes((ServerRequest) any(),
                (org.springframework.boot.web.error.ErrorAttributeOptions) any());
    }
}

