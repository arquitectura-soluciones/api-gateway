package mx.santander.gateway.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

class GlobalErrorAttributesTest {
    @Test
    void testConstructor() {
        GlobalErrorAttributes actualGlobalErrorAttributes = new GlobalErrorAttributes();
        actualGlobalErrorAttributes.setMessage("Not all who wander are lost");
        actualGlobalErrorAttributes.setStatus(HttpStatus.CONTINUE);
        assertEquals("Not all who wander are lost", actualGlobalErrorAttributes.getMessage());
        assertEquals(HttpStatus.CONTINUE, actualGlobalErrorAttributes.getStatus());
    }
}

