package mx.santander.gateway;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import lombok.extern.slf4j.Slf4j;
import mx.santander.gateway.service.IFirmarJwtService;

/***
 * Clase que inicia la aplicacion
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
@SpringBootApplication
@Slf4j
@EnableCaching
public class GatewayApplication implements InitializingBean {

	/***
	 * Nombre de la aplicacion
	 */
	@Value("${spring.application.name:api-gateway}")
	private String appName;

	/** Instancia para carga la llave privada */
	@Autowired
	private IFirmarJwtService firmarJwtService;
	
	/***
	 * Metodo inicial del api-gateway
	 * @param args Argumentos de incio
	 */
	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}

	/***
	 * Metodo que imprime al log
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		log.info("init:{}", appName);
		firmarJwtService.getRSAJwt();
	}

}
