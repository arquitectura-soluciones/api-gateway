package mx.santander.gateway.service;

import org.springframework.web.server.ServerWebExchange;

import mx.santander.gateway.model.ErrorHandlerDTO;

/**
 * 
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
public interface IRequestIsOkService {
	/***
	 * Metodo que valida si la peticion se encuentra formada de manera correcta
	 * 
	 * @param exchange Peticion
	 * @return ErrorHandler
	 */
	ErrorHandlerDTO requestIsOk(ServerWebExchange exchange);
}
