package mx.santander.gateway.service;

import org.springframework.web.server.ServerWebExchange;

import mx.santander.gateway.model.ErrorHandlerDTO;
import reactor.core.publisher.Mono;

/***
 * Clase que mapea el error
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
public interface ICustomErrorService {
	/****
	 * Metodo con el error de la Respuesta
	 * @param exchange Peticion
	 * @param dataErrorResponsex Tipo de Error
	 * @return Vista con error
	 */
	Mono<Void> errorResponse(ServerWebExchange exchange, ErrorHandlerDTO dataErrorResponsex);
}
