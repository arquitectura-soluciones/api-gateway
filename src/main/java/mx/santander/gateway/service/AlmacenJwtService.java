package mx.santander.gateway.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mx.santander.gateway.model.DataJwtDTO;

/***
 * Clase que almacena en memoria los datos del JWT
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
@Slf4j
@Service
public class AlmacenJwtService implements IAlmacenJwtService {
	/***
	 * Lista de informacion del jwt
	 */
	@Autowired(required=false)
	public List<DataJwtDTO> JwtAlmacenados = new ArrayList<>();

	/***
	 * Metodo que indica si los datos del JWT se encuentra almacenado
	 * @param DataJwtDTO datos del JWT que se almacena en memoria
	 * @return DataJwtDTO datos que coincide
	 */
	@Override
	public DataJwtDTO estaGuardado(DataJwtDTO data) {
		DataJwtDTO result = null;
		Optional<DataJwtDTO> dataFound = findJwt(JwtAlmacenados, data);
		if (dataFound.isPresent()) {
			DataJwtDTO datoExistente=dataFound.get();
			Date generado=datoExistente.getIssuedAt();
			Date ahora=new Date();
			long diff = ahora.getTime() - generado.getTime();
			long diffSeconds = diff / 1000;  
			log.info("Se encontraron los datos "+datoExistente.toString()+" intervalo: "+diffSeconds);
			if(diffSeconds<=180){
				log.info("Datos en el intervalo de tiempo");
				return datoExistente;
			}else{
				eliminar(datoExistente);
				log.error("Los datos almacenados ya caducaron, eliminando..");
			}
		}else {
			log.info("No, se encontraron, los datos "+data.toString());
		}
		return result;
	}
	
	/***
	 * Metodo que elimina los datos del JWT, si se encuentra almacenado
	 * @param DataJwtDTO datos del JWT que se elimina de memoria
	 */
	@Override
	public void eliminar(DataJwtDTO data) {
		log.info("Eliminando:DataJwtDTO "+data.toString());
		JwtAlmacenados.removeIf(x -> x.getAccessToken().equals(data.getAccessToken()));
	}

	/***
	 * Metodo que guarda los datos del JWT
	 * @param DataJwtDTO datos del JWT que seran almacenados en memoria
	 */
	@Override
	public void guardar(DataJwtDTO data) {
		log.info("Guardando:DataJwtDTO "+data.toString());
		JwtAlmacenados.add(data);
	}

	/***
	 * Metodo que realiza la busqueda de los datos del JWT
	 * @param DataJwtDTO datos del JWT a buscar
	 */
	@Override
	public final Optional<DataJwtDTO> findJwt(Collection<DataJwtDTO> yourList, DataJwtDTO data) {
		return yourList.stream()
				.filter(c -> c.getAccessToken().equals(data.getAccessToken()))
				.findAny();
	}

}
