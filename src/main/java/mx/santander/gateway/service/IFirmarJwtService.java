package mx.santander.gateway.service;

/***
 * Interfaz que permite filtrar el JWT
 * 
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
public interface IFirmarJwtService {
	/***
	 * obtiene/carga la llave
	 */
	void getRSAJwt();

	/**
	 * Metodo que Firma el jwt
	 * 
	 * @return JWT firmado
	 */
	String sign(String Subject, String Issuer, String Audience, String Kid);
	/**
	 * Metodo que Firma el jwt
	 * 
	 * @return JWT firmado
	 */
	String sign(String Subject);

	/***
	 * Metodo que indica si la llave privada se encuentra, cargada de manera exitosa
	 * 
	 * @return true, si la llave privada esta Ok
	 */
	boolean iskeyLoaded();
}
