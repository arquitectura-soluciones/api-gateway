package mx.santander.gateway.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMException;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

/***
 * Clase que permite la firma del JWT
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
@Slf4j
@Service
public class FirmarJwtService implements IFirmarJwtService {
	/***
	 * Kid necesario para la firma del JWT
	 */
	@Value("${jwtData.kid:api-gateway}")
	private String Kid;

	/***
	 * Audience necesario para la firma del JWT
	 */
	@Value("${jwtData.audience:api-gateway}")
	private String Audience;

	/***
	 * Issuer necesario para la firma del JWT
	 */
	@Value("${jwtData.issuer:sant_gateway_mx_v1}")
	private String Issuer;

	/***
	 * llavePrivada necesario para la firma del JWT
	 */
	@Autowired(required = false)
	public PrivateKey llavePrivada = null;

	/***
	 * Indica si la llave se cargo de manera correcta
	 */
	@Autowired(required = false)
	private boolean keyLoaded = false;

	/***
	 * Ruta de la llave privada
	 */
	@Value("${pathPrivateKey:/etc/keys/private-key.pem}")
	private String rutaLlavePrivada;

	/***
	 * Metodo que valida si la llave privada esta cargada
	 */
	public boolean iskeyLoaded() {
		return keyLoaded;
	}

	/***
	 * Metodo que permite cargar la llave privada
	 */
	public void getRSAJwt() {		
		File fileLlavePrivada = getFile(rutaLlavePrivada);
		String format = getFormatKey(fileLlavePrivada);
		log.info("Formato de la llave: " + format);
		llavePrivada = getKey(fileLlavePrivada, format);
		if (llavePrivada != null) {
			keyLoaded = true;
			log.info("Tipo de llave: " + llavePrivada.getFormat());
		}
	}

	/***
	 * Metodo que permite firmar el jwt
	 */
	public String sign(String Subject) {
		return sign(Subject, Issuer, Audience, Kid);
	}

	/***
	 * Metodo que permite firmar el jwt
	 */
	public String sign(String Subject, String Issuer, String Audience, String Kid) {
		log.info("Generando JWT, datos " + Subject);
		Instant now = Instant.now();
		Map<String, Object> headerMap = new HashMap<String, Object>();
		headerMap.put("alg", "RS256");
		headerMap.put("typ", "JWT");
		headerMap.put("kid", Kid);
		String abc=UUID.randomUUID().toString();
		return Jwts.builder().setHeader(headerMap).setAudience(Audience)
				.setNotBefore(Date.from(now.minus(5, ChronoUnit.MINUTES))).setIssuedAt(Date.from(now))
				.setExpiration(Date.from(now.plus(5, ChronoUnit.MINUTES))).setIssuer(Issuer).setSubject(Subject)
				.setId(abc).signWith(llavePrivada, SignatureAlgorithm.RS256).compact();
	}

	/**
	 * Metodo que lee la llave privada
	 * 
	 * @param ruta ruta de la llave privada
	 * @return File
	 */
	public File getFile(String ruta) {
		log.info("Leyendo la llave en: " + ruta);
		return new File(ruta);
	}

	/**
	 * Metodo que obtiene el formato de la llave privada
	 * 
	 * @param llavePrivada llavePrivada
	 * @return String formato
	 */
	public String getFormatKey(File llavePrivada) {
		String format = "";
		try {
			String key = new String(Files.readAllBytes(llavePrivada.toPath()), Charset.defaultCharset());
			if (key.contains("BEGIN PRIVATE KEY")) {
				format = "OCHO";
			} else if (key.contains("BEGIN RSA PRIVATE KEY")) {
				format = "UNO";
			} else {
				format = "UNKNOWN";
			}
		} catch (IOException e) {
			log.error("IOException:getFormatKey:privateKey", e);
		}
		return format;
	}

	/***
	 * PrivateKey
	 * 
	 * @param llavePrivada llavePrivada
	 * @param format       format
	 * @return PrivateKey
	 */
	public PrivateKey getKey(File llavePrivada, String format) {
		PrivateKey result = null;
		switch (format) {
		case "OCHO":
			result = formatPkcsOcho(llavePrivada);
			break;
		case "UNO":
			result = formatPkcsUno(llavePrivada);
			break;
		default:
			result = formatPkcsUno(llavePrivada);
			break;
		}
		return result;
	}

	/***
	 * PrivateKey PrivateKey
	 * 
	 * @param file file
	 * @return PrivateKey
	 */
	public PrivateKey formatPkcsUno(File file) {
		Security.addProvider(new BouncyCastleProvider());
		JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
		PEMParser pemParser = null;
		PrivateKey pk = null;
		try {
			pemParser = new PEMParser(new FileReader(file));
			try {
				Object object = pemParser.readObject();
				KeyPair kp = converter.getKeyPair((PEMKeyPair) object);
				pk = kp.getPrivate();
			} catch (NullPointerException e) {
				log.error("IOException:formatPkcsUno:privateKey", e);
			} catch (PEMException e) {
				log.error("PEMException:formatPkcsUno:privateKey", e);
			} catch (IOException e) {
				log.error("IOException:formatPkcsUno:privateKey", e);
			}
		} catch (FileNotFoundException e) {
			log.error("FileNotFoundException:formatPkcsUno:privateKey", e);
		} finally {
			if (pemParser != null) {
				try {
					pemParser.close();
				} catch (IOException e) {
					log.error("pemParser:IOException", e);
				}
			}
		}
		return pk;
	}

	/***
	 * formatPkcsOcho
	 * 
	 * @param file file
	 * @return PrivateKey
	 */
	public PrivateKey formatPkcsOcho(File file) {
		String key = null;
		PrivateKey response = null;
		try {
			key = new String(Files.readAllBytes(file.toPath()), Charset.defaultCharset());
			String privateKeyPEM = key.replace("-----BEGIN PRIVATE KEY-----", "").replaceAll(System.lineSeparator(), "")
					.replace("-----END PRIVATE KEY-----", "");			
			byte[] encoded = Base64.getDecoder().decode(privateKeyPEM);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
			response = keyFactory.generatePrivate(keySpec);
		} catch (InvalidKeySpecException e) {
			log.error("InvalidKeySpecException:formatPkcsOcho:privateKey", e);
		} catch (NoSuchAlgorithmException e) {
			log.error("NoSuchAlgorithmException:formatPkcsOcho:privateKey", e);
		} catch (IOException e) {
			log.error("IOException:formatPkcsOcho:privateKey", e);
		}
		return response;
	}

}
