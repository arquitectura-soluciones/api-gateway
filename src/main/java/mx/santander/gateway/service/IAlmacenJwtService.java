package mx.santander.gateway.service;

import java.util.Collection;
import java.util.Optional;

import mx.santander.gateway.model.DataJwtDTO;

/***
 * Clase que almacena en memoria los datos del JWT
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
public interface IAlmacenJwtService {
	/***
	 * Metodo que guarda los datos del JWT
	 * @param DataJwtDTO datos del JWT que seran almacenados en memoria
	 */
	void guardar(DataJwtDTO data);
	
	/***
	 * Metodo que indica si los datos del JWT se encuentra almacenado
	 * @param DataJwtDTO datos del JWT que se almacena en memoria
	 * @return DataJwtDTO datos que coincide
	 */
	DataJwtDTO estaGuardado(DataJwtDTO data);
	
	/***
	 * Metodo que elimina los datos del JWT, si se encuentra almacenado
	 * @param DataJwtDTO datos del JWT que se elimina de memoria
	 */
	void eliminar(DataJwtDTO data);
	
	
	/***
	 * Metodo que realiza la busqueda de los datos del JWT
	 * @param DataJwtDTO datos del JWT a buscar
	 */
	Optional<DataJwtDTO> findJwt(Collection<DataJwtDTO> yourList, DataJwtDTO data);
}
