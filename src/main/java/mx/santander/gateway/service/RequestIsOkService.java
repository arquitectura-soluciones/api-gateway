package mx.santander.gateway.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerWebExchange;

import lombok.extern.slf4j.Slf4j;
import mx.santander.gateway.model.ErrorHandlerDTO;

/***
 * Clase que valida si la peticion se encuentra ok
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
@Slf4j
@Service
public class RequestIsOkService implements IRequestIsOkService{
	/**
	 * Clase con la validacion del AccessToken
	 */
	@Autowired
	private IValidateAccessTokenService validaAccessToken;

	/**
	 * Clase con la validacion del AccessToken
	 */
	@Autowired
	private IFirmarJwtService firmarJwt;

	/***
	 * Metodo que valida si la peticion se encuentra formada de manera correcta
	 * @param exchange Peticion
	 * @return ErrorHandler
	 */
	public ErrorHandlerDTO requestIsOk(ServerWebExchange exchange) {
		String routeQuery=exchange.getRequest().getURI().getRawQuery();
		String routePath=exchange.getRequest().getURI().getPath();
		log.info("Filtrando la route "+routePath+" withQueryParams: "+routeQuery);
		
		ErrorHandlerDTO estado = null;
		String accessToken = validaAccessToken.getAccessTokenFromRequest(exchange);
		log.info("accessToken " + accessToken);

		String tipoAccessToken = validaAccessToken.tipoAccessToken(accessToken);
		if (tipoAccessToken == null) {
			estado = new ErrorHandlerDTO();
			estado.setCause("invalid_access_token");
			estado.setMessage("prefix_access_token_invalid");
		}
		
		String iss=validaAccessToken.getIssFromRequest(exchange);
		if (iss == null) {
			estado = new ErrorHandlerDTO();
			estado.setCause("invalid_scope");
			estado.setMessage("invalid_scope");
		}

		if (accessToken == null) {
			log.info("accessToken no definido");
			estado = new ErrorHandlerDTO();
			estado.setCause("no_access_token");
			estado.setMessage("no_access_token");
		}
		
		String clientId = validaAccessToken.getClientIdFromRequest(exchange);
		if (clientId == null) {
			estado = new ErrorHandlerDTO();
			estado.setCause("no_client_id");
			estado.setMessage("x-ibm-client-id");
		}
		
		if (!firmarJwt.iskeyLoaded()) {
			log.info("Error al consumir el servicio, la llave no se encuentra cargada de manera exitosa");
			estado = new ErrorHandlerDTO();
			estado.setCause("error_private_key");
			estado.setMessage("error_private_key");
		}
		return estado;
	}
	


}
