package mx.santander.gateway.service;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import lombok.extern.slf4j.Slf4j;
import mx.santander.gateway.model.ErrorHandlerDTO;
import mx.santander.gateway.model.ResponseIntrospectDTO;

/***
 * clase que consume el servicio de Instrospect para validar que el access token
 * enviado se encuentra ok
 * 
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
@Slf4j
@Service
public class IntrospectService implements IIntrospectService {

	/***
	 * Url del endPoint instrospect para aplicaciones internas
	 */
	@Value("${urlOauthIN}")
	private String urlOauthIN;

	/***
	 * Url del endPoint instrospect para personas fisicas
	 */
	@Value("${urlOauthPF}")
	private String urlOauthPF;

	/***
	 * RestOperations de tipo template
	 */
	@Autowired
	private RestOperations oauthServerTemplate;

	/***
	 * Url del servidor Instrospect
	 * 
	 * @param tipoAccessToken
	 * @return
	 */
	public String urlInstrospect(String tipoAccessToken) {
		String result = urlOauthIN;
		if (tipoAccessToken.equalsIgnoreCase("PF")) {
			result = urlOauthPF;
		}
		return result;
	}

	/***
	 * Metodo que permite el consumo del servicio Introspect
	 * @param Jwt jwt a inyectar en la peticion, el cual fue firmado por la llave privada
	 * @param AccessToken token de acceso, proporcionado por el cliente consumidor
	 * @param Scope scope del cliente
	 * @param tipoAccessToken prefijo del accessToken, permite validar si es persona fisica o app interna
	 */
	@Override
	public Object callInstrospect(String Jwt, String AccessToken, String Scope, String tipoAccessToken) {

		try {
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content_Type", "application/x-www-form-urlencoded");
			headers.add(AUTHORIZATION, "Bearer " + Jwt);
			headers.add("need-jwt", "yes");

			LinkedMultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();
			body.add("token", AccessToken);
			body.add("token_type_hint", "access_token");
			body.add("iss", Scope);

			HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<LinkedMultiValueMap<String, Object>>(
					body, headers);
			String server = urlInstrospect(tipoAccessToken);
			log.info("Invocando al instrospect " + server + " iss " + Scope);
			ResponseEntity<ResponseIntrospectDTO> response = oauthServerTemplate.postForEntity(
					server,
					requestEntity,
					ResponseIntrospectDTO.class,
					String.class);

			log.info("Post_check:token response: " + response.getStatusCodeValue() + " " + response.getBody().toString());
			return response.getBody();
		} catch (HttpClientErrorException | HttpServerErrorException ex) {
			log.error("Error contacting with oauth server", ex);
			ErrorHandlerDTO abc = new ErrorHandlerDTO();
			abc.setCause("instrospect_" + ex.getRawStatusCode() + "_communication_error");
			abc.setMessage(ex.getMessage() + " - " + ex.getStatusText() + " - " + ex.getResponseBodyAsString());
			return abc;
		} catch (RestClientException e) {
			log.error("Error contacting with oauth server", e);
			ErrorHandlerDTO abc = new ErrorHandlerDTO();
			abc.setCause("instrospect_communication_error");
			abc.setMessage(e.getMessage());
			return abc;
		}
	}
}
