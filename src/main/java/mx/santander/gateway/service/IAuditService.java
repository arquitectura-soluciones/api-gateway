package mx.santander.gateway.service;

import org.springframework.web.server.ServerWebExchange;

import mx.santander.gateway.filter.FilterFactory.ArgsFiltersDTO;

/**
 * Interfaz para crear las pistas de auditoria
 * 
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
public interface IAuditService {
	/***
	 * Metodo a auditar
	 * @param ServerWebExchange peticion a auditar
	 */
	public void audit(ServerWebExchange exchange, String userProfile);
	/***
	 * Metodo a auditar
	 * @param ServerWebExchange peticion a auditar
	 */
	public void audit(ServerWebExchange exchange, ArgsFiltersDTO subject);
	/***
	 * Metodo a auditar
	 * @param ServerWebExchange peticion a auditar
	 */
	public void audit(ServerWebExchange exchange);
}
