package mx.santander.gateway.exception;

import java.net.UnknownHostException;
import java.util.Map;

import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;

import lombok.extern.slf4j.Slf4j;

/***
 * Clase que mapea las excepciones en el consumo del servicio
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
@Component
@Slf4j
public class GlobalErrorAttributes extends DefaultErrorAttributes {

	
	/**
	 * Estatus de la peticion, en caso de la excepcion
	 */
	private HttpStatus status = HttpStatus.UNAUTHORIZED;
	
	/***
	 * Mensaje de error de la excepcion
	 */
	private String message = "Error";

	/***
	 * Cuerpo de la peticion, en caso de error
	 */
	@Override
	public Map<String, Object> getErrorAttributes(ServerRequest request, ErrorAttributeOptions options) {
	    Throwable error = getError(request);
	    log.error(error.getMessage()+" - "+error.getCause()+" - "+error.toString()+" - "+error.getClass().getName());
		Map<String, Object> map = super.getErrorAttributes(request, options);
		map.put("status", getStatus());
    	map.put("message", getMessage());	
	    if (error instanceof UnknownHostException) {
			map.put("error", error.getClass().getName());	
	    	map.put("message", error.toString());	
	    } 
		return map;
	}

	/**
	 * Retorna el estatus de la peticion
	 * 
	 * @return the status
	 */
	public HttpStatus getStatus() {
		return status;
	}

	/**
	 * Asigna el estado de la peticion
	 * 
	 * @param status the status to set
	 */
	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	/**
	 * Obtiene el mensaje de la peticion
	 * 
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Setea el msj de la peticion
	 * 
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}
