package mx.santander.gateway.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import mx.santander.gateway.service.IAuditService;

/***
 * Clase que permite obtener las propiedades del archivo de configuración
 * 
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
@Component
public class FilterFactory extends AbstractGatewayFilterFactory<FilterFactory.ArgsFiltersDTO> {
	/**
	 * Variable con el nombre de los filtros, en el archivo de configuración
	 */
	private static final String FILTER_OAUTH_NAME = "OauthSecurity";

	/**
	 * Clase con para almacenar la auditoria
	 */
	@Autowired
	private IAuditService auditService;

	/**
	 * Constructor de la clase
	 */
	public FilterFactory() {
		super(ArgsFiltersDTO.class);
	}

	/**
	 * Metodo que lee las propiedades ArgsFiltersDTO del filter
	 */
	@Override
	public GatewayFilter apply(ArgsFiltersDTO config) {
		return (exchange, chain) -> {
			auditService.audit(exchange, config);
			ServerHttpRequest request = exchange.getRequest().mutate()
					.headers(httpHeaders -> httpHeaders.set("iss", config.getScope())).build();
			return chain.filter(exchange.mutate().request(request).build());
		};
	}

	/**
	 * Propiedades a leer
	 */
	@Override
	public String name() {
		return FILTER_OAUTH_NAME;
	}

	/***
	 * Clase que define los propiedades
	 * 
	 * @author Miguel Angel Garcia Labastida - Z045353
	 * @author Omar Barrera Valentin - Z259151
	 */
	@Setter
	@Getter
	public static class ArgsFiltersDTO {
		private String scope;
	}

}