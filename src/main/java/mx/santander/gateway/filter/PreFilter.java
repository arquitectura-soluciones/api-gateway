package mx.santander.gateway.filter;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import lombok.extern.slf4j.Slf4j;
import mx.santander.gateway.model.DataJwtDTO;
import mx.santander.gateway.model.ErrorHandlerDTO;
import mx.santander.gateway.model.ResponseIntrospectDTO;
import mx.santander.gateway.service.IAlmacenJwtService;
import mx.santander.gateway.service.IAuditService;
import mx.santander.gateway.service.ICustomErrorService;
import mx.santander.gateway.service.IFirmarJwtService;
import mx.santander.gateway.service.IIntrospectService;
import mx.santander.gateway.service.IRequestIsOkService;
import mx.santander.gateway.service.IValidateAccessTokenService;
import reactor.core.publisher.Mono;

/***
 * Clase que valida las peticiones entrantes-salientes
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
@Component
@Slf4j
public class PreFilter implements GlobalFilter {
	/** Instancia para usar la interface de IRequestIsOkService */
	@Autowired
	private IRequestIsOkService RequestIsOkService;

	/** Instancia para usar la interface de ICustomErrorService */
	@Autowired
	private ICustomErrorService customErrorService;

	/** Instancia para usar la interface de IIntrospectService */
	@Autowired
	private IIntrospectService introspectServ;

	/** Instancia para usar la interface de IFirmarJwtService */
	@Autowired
	private IFirmarJwtService firmarJwt;

	/**
	 * Clase con la validacion del AccessToken
	 */
	@Autowired
	private IValidateAccessTokenService validaAccessToken;
	
	/**
	 * Clase con la validacion del almacen del Jwt
	 */
	@Autowired
	private IAlmacenJwtService almacenJwt;
	
	/**
	 * Clase para almacenar la auditoria
	 */
	@Autowired
	private IAuditService auditService;

	/***
	 * Metodo que valida la peticion que pasara al servicio destino
	 * @param ServerWebExchange peticion
	 * @param GatewayFilterChain peticion chain
	 */
	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		ErrorHandlerDTO requestIsOk = RequestIsOkService.requestIsOk(exchange);
		if (requestIsOk != null) {
			return customErrorService.errorResponse(exchange, requestIsOk);
		}
		String subject = validaAccessToken.getClientIdFromRequest(exchange);
		String accessToken = validaAccessToken.getAccessTokenFromRequest(exchange);
		String tipoAccessToken=validaAccessToken.tipoAccessToken(accessToken);
		String scope = validaAccessToken.getIssFromRequest(exchange);
	
		//Se forman los datos del JWT
		Instant now = Instant.now();
		DataJwtDTO dataRequestValidation=new DataJwtDTO();
		dataRequestValidation.setIssuer(scope);
		dataRequestValidation.setSubject(subject);
		dataRequestValidation.setScope(scope);
		dataRequestValidation.setClientId(subject);
		dataRequestValidation.setTipoPersona(tipoAccessToken);
		dataRequestValidation.setMetodo(exchange.getRequest().getMethod());
		dataRequestValidation.setEndPoint(exchange.getRequest().getURI().getPath());
		dataRequestValidation.setAccessToken(accessToken);
		log.info("Buscando los datos "+dataRequestValidation.toString());

		DataJwtDTO estaG=almacenJwt.estaGuardado(dataRequestValidation);
		String jwt=null;
		if(estaG==null) {
			//No hay datos guardados, consumiendo al instrospect para obtener el jwt
			String firmaCnLlavePrivada = firmarJwt.sign(subject);
			Object responseIntrospect=introspectServ.callInstrospect(firmaCnLlavePrivada, accessToken, scope,tipoAccessToken);
			if(responseIntrospect.getClass()==ErrorHandlerDTO.class) {
				// Hubo un error al llamar al introspect
				return customErrorService.errorResponse(exchange, (ErrorHandlerDTO)responseIntrospect);
			}else {
				ResponseIntrospectDTO res=(ResponseIntrospectDTO)responseIntrospect;
				jwt=res.getJwt();
				dataRequestValidation.setJwt(jwt);
				dataRequestValidation.setNotBefore(Date.from(now.minus(5, ChronoUnit.MINUTES)));
				dataRequestValidation.setIssuedAt(Date.from(now));
				dataRequestValidation.setExpiration(Date.from(now.plus(5, ChronoUnit.MINUTES)));
				almacenJwt.guardar(dataRequestValidation);
			}
		}else{
			//Si hay datos guardados, usando el jwt 
			jwt=estaG.getJwt();
		}
		ServerHttpRequest mutatedRequest = exchange.getRequest().mutate().header(HttpHeaders.AUTHORIZATION, "Bearer " + jwt).build();
		exchange= exchange.mutate().request(mutatedRequest).build();
		log.info("Global Pre Filter executed " + scope);
		auditService.audit(exchange);
		return chain.filter(exchange);
	}
}