package mx.santander.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

/***
 * Clase que configura el RestTemplate
 * 
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
@Configuration
public class OAuthConfig {

	/***
	 * Metodo que retorna el RestTemplate
	 * 
	 * @return restTemplate
	 */
	@Bean
	public RestOperations oauthServerTemplate() {
		return new RestTemplate();
	}
}