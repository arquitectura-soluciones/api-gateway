package mx.santander.gateway.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Clase que mapea la respuesta del Introspect
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
@Getter
@Setter
@ToString
public class ResponseIntrospectDTO implements Serializable{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * jwt, respuesta del introspect
	 */
	private String jwt;
}
