package mx.santander.gateway.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.http.HttpMethod;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/***
 * Clase con los datos del Jwt
 * 
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
@Getter
@Setter
@ToString
public class DataJwtDTO implements Serializable {
	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Subject, que sera almacenado en los datos del JWT
	 */
	private String subject;
	/**
	 * issuer, que sera almacenado en los datos del JWT
	 */
	private String issuer;
	/**
	 * Audience, que sera almacenado en los datos del JWT
	 */
	private String audience;
	/**
	 * kid, que sera almacenado en los datos del JWT
	 */
	private String kid;
	/**
	 * clientId, que sera almacenado en los datos del JWT
	 */
	private String clientId;
	/**
	 * scope, que sera almacenado en los datos del JWT
	 */
	private String scope;
	/**
	 * endPoint, que sera almacenado en los datos del JWT
	 */
	private String endPoint;
	/**
	 * jwt, que sera almacenado en los datos del JWT
	 */
	private String jwt;
	/**
	 * tipoPersona, que sera almacenado en los datos del JWT
	 */
	private String tipoPersona;
	/**
	 * notBefore, que sera almacenado en los datos del JWT
	 */
	private Date notBefore;
	/**
	 * issuedAt, que sera almacenado en los datos del JWT
	 */
	private Date issuedAt;
	/**
	 * Expiration, que sera almacenado en los datos del JWT
	 */
	private Date expiration;
	/**
	 * HttpMethod, que sera almacenado en los datos del JWT
	 */
	private HttpMethod metodo;

	/*
	* HttpMethod, que sera almacenado en los datos del JWT
	*/
   private String accessToken;

	/**
	 * Constructor por default de la clase
	 */
	public DataJwtDTO() {
		super();
	}

	/**
	 * Constructor con los datos basicos del Jwt
	 * 
	 * @param subject
	 * @param tipoPersona
	 * @param endPoint
	 */
	public DataJwtDTO(String subject, String tipoPersona, String endPoint) {
		this.subject = subject;
		this.tipoPersona = tipoPersona;
		this.endPoint = endPoint;
	}
}
