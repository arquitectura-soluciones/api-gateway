package mx.santander.gateway.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/***
 * Datos que mapean el errorHandler
 * @author Miguel Angel Garcia Labastida - Z045353
 * @author Omar Barrera Valentin - Z259151
 */
@Getter
@Setter
public class ErrorHandlerDTO implements Serializable{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/***
	 * Mensaje del error
	 */
	private String message;
	/***
	 * Causa del error
	 */
	private String cause;
}
